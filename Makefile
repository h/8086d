WRN := -Wall -Wextra
INF := main.c
OUT := 8086d

ASM-INF1 := listing_0037_single_register_mov.asm
ASM-INF2 := listing_0038_many_register_mov.asm
ASM-OUT1 := listing_0037_single_register_mov
ASM-OUT2 := listing_0038_many_register_mov

default:
	$(CC) $(WRN) $(INF) -o $(OUT)

testdata:
	nasm $(ASM-INF1) -o $(ASM-OUT1)
	nasm $(ASM-INF2) -o $(ASM-OUT2)

test: clean testdata default
	./$(OUT) $(ASM-OUT1) > t1.S
	./$(OUT) $(ASM-OUT2) > t2.S
	nasm t1.S -o t1
	nasm t2.S -o t2
	diff t1 $(ASM-OUT1)
	diff t2 $(ASM-OUT2)

clean:
	rm -f t1.S t2.S t1 t2 $(OUT) $(ASM-OUT1) $(ASM-OUT2)

